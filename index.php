<?php
include "session.php";
include "dbconfig.php";

$sql = "SELECT firstname, lastname, email, address, phone from register";
$query = mysql_query($sql);
$data = mysql_fetch_array($query);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Profile</title>
    <link rel="stylesheet" href="style2.css">
</head>
<body>
    <div class="container">
        <div class="banner">
            <label for="" class="username"><?php echo ucfirst($_SESSION['username']); ?></label>
            <a href="logout.php" class="logout">Logout</a>
            <h2>User Profile</h2>
            
        </div>
        <div class="sidebar">
            <label for=""><a href="#">Profile</a></label><br><br>
            <label for=""><a href="#">Education</a></label><br><br>
            <label for=""><a href="#">Profession</a></label><br><br>
            <label for=""><a href="#">Interest</a></label> <br><br><br>
            <label for=""><a href="#">Edit Profile</a></label><br><br>
            <label for=""><a href="#">Edit Education</a></label><br><br>
            <label for=""><a href="#">Edit Profession</a></label><br><br>
            <label for=""><a href="#">Edit Interest</a></label><br><br>
        </div>
        <div class="profile">
            <label for="">Name:</label><label for="" class="field"><?php echo $data['firstname']; ?></label><br><br>
            <label for="">Address:</label><label for="" class="field"><?php echo $data['address']; ?></label><br><br>
            <label for="">Mobile:</label><label for="" class="field"><?php echo $data['phone']; ?></label><br><br>
            <label for="">Email:</label><label for="" class="field"><?php echo $data['email']; ?></label>
        </div>

    </div>
</body>
</html>
